//
//  AppDelegate.h
//  idiocy diagnostic test
//
//  Created by jenn on 7/17/17.
//  Copyright © 2017 jgordo13. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

