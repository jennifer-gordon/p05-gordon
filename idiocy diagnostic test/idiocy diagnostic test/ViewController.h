//
//  ViewController.h
//  idiocy diagnostic test
//
//  Created by jenn on 7/17/17.
//  Copyright © 2017 jgordo13. All rights reserved.
//

#import <UIKit/UIKit.h>

int idiotScore = 0;

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *cont;

@property (strong, nonatomic) IBOutlet UIButton *about;

@property (strong, nonatomic) IBOutlet UIButton *donate;

@property (strong, nonatomic) IBOutlet UIButton *score;




@property (strong, nonatomic) IBOutlet UISegmentedControl *beer;

@property (strong, nonatomic) IBOutlet UISegmentedControl *nigerian;

@property (strong, nonatomic) IBOutlet UISegmentedControl *grass;

@property (strong, nonatomic) IBOutlet UISegmentedControl *hipsters;

@property (strong, nonatomic) IBOutlet UISegmentedControl *babies;

@property (strong, nonatomic) IBOutlet UISegmentedControl *hospitalized;

@property (strong, nonatomic) IBOutlet UISegmentedControl *microwave;



@property (nonatomic, strong) IBOutlet UILabel *scoretotal;

@property (strong, nonatomic) IBOutlet UILabel *yesnoidiot;

 -(IBAction)scoreCalc:(id)sender;

@end

